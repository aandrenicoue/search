package com.example.finder.controller;

import com.example.finder.model.ObjetPerdu;
import com.example.finder.service.ObjetPerduService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/objetperdus")
public class ObjetPerduController {

    @Autowired
    private ObjetPerduService objetPerduService;

    @GetMapping
    public String getAllObjetPerdus(Model model) {
        List<ObjetPerdu> objetPerdus = objetPerduService.getAllObjetPerdus();
        model.addAttribute("objetperdus", objetPerdus);
        return "index";
    }

    @GetMapping("/new")
    public String createObjetPerduForm(Model model) {
        model.addAttribute("objetperdu", new ObjetPerdu());
        return "objet-perdu-form";
    }

    @PostMapping
    public String saveProduct(@ModelAttribute("objetperdu") ObjetPerdu objetperdu) {
        objetPerduService.saveObjetPerdu(objetperdu);
        return "redirect:/objetperdus";
    }


}
