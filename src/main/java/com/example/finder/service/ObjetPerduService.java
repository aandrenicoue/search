package com.example.finder.service;


import com.example.finder.model.ObjetPerdu;
import com.example.finder.repository.ObjetPerduRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ObjetPerduService {

    @Autowired
    private ObjetPerduRepository objetPerduRepository;

    public List<ObjetPerdu> getAllObjetPerdus(){
        return objetPerduRepository.findAll();
    }

    public ObjetPerdu getObjetPerduById(Long id){
        return  objetPerduRepository.findById(id).orElse(null);
    }

    public ObjetPerdu saveObjetPerdu(ObjetPerdu objetPerdu){
        return objetPerduRepository.save(objetPerdu);
    }

    public void deleteObjetPerdu(Long id) {
        objetPerduRepository.deleteById(id);
    }
}
