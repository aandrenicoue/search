package com.example.finder.repository;

import com.example.finder.model.ObjetPerdu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ObjetPerduRepository extends JpaRepository<ObjetPerdu, Long> {
}
