package com.example.finder.model;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
public class ObjetPerdu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String libelle;
    private String lieuDePerte;
    private String nomDetenteur;
    private String dateDePerte;
    @Column(columnDefinition = "TEXT")
    private String descriptionObjet;
    private String contact;
    private int status;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLieuDePerte() {
        return lieuDePerte;
    }

    public void setLieuDePerte(String lieuDePerte) {
        this.lieuDePerte = lieuDePerte;
    }

    public String getNomDetenteur() {
        return nomDetenteur;
    }

    public void setNomDetenteur(String nomDetenteur) {
        this.nomDetenteur = nomDetenteur;
    }

    public String getDateDePerte() {
        return dateDePerte;
    }

    public void setDateDePerte(String dateDePerte) {
        this.dateDePerte = dateDePerte;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDescriptionObjet() {
        return descriptionObjet;
    }

    public void setDescriptionObjet(String descriptionObjet) {
        this.descriptionObjet = descriptionObjet;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @PrePersist
    protected void onCreate() {
        createdAt = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedAt = LocalDateTime.now();
    }

}
